package main

// DOCS AND REPO: https://gitlab.com/benyanke/docker-fsspx-today-ical

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/ReneKroon/ttlcache/v2"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jordic/goics"
	log "github.com/sirupsen/logrus"

	"github.com/araddon/dateparse"
)

var base_url = "https://fsspx.today/chapel"

// Cache responses from fsspx.today
var notFound = ttlcache.ErrNotFound
var Cache ttlcache.SimpleCache

// From https://mholt.github.io/json-to-go/
type Event struct {
}

type CalData struct {
	Timezone   string
	Chapel     string
	FetchedUrl string
	Seasons    []struct {
		ID        int    `json:"id"`
		Name      string `json:"name"`
		Content   string `json:"content"`
		Permalink string `json:"permalink"`
		Start     string `json:"start"`
		StartTS   int    `json:"startTS"`
		End       string `json:"end"`
		EndTS     int    `json:"endTS"`
	} `json:"seasons"`
	MassDays []struct {
		Day          string `json:"day"`
		DayTS        int    `json:"dayTS"`
		DayYMD       string `json:"dayYMD"`
		DayDMJ       string `json:"dayDMJ"`
		DayATCFormat string `json:"dayATCFormat"`
		Masses       []struct {
			Time        string `json:"time"`
			TimeATC     string `json:"timeATC"`
			Description string `json:"description"`
			Skip        bool   `json:"skip"`
		} `json:"masses"`
		Message string `json:"message,omitempty"`
	} `json:"massDays"`
	MassTimesString           string `json:"massTimesString"`
	EditPrintSelectionString  string `json:"editPrintSelectionString"`
	ParishChapelName          string `json:"parishChapelName"`
	ParishAddress             string `json:"parishAddress"`
	ParishPlace               string `json:"parishPlace"`
	SimplifiedLogo            bool   `json:"simplifiedLogo"`
	DisplayAppDescriptionIcon bool   `json:"displayAppDescriptionIcon"`
	DisplayPrintIcon          bool   `json:"displayPrintIcon"`
	PrintSelectionCalendar    bool   `json:"printSelectionCalendar"`
}

func main() {

	// Cache responses from fsspx.today
	Cache = ttlcache.NewCache()
	// Cache for 6 hours
	Cache.SetTTL(time.Duration(24 * time.Hour))

	log.WithFields(log.Fields{
		"port": 8080,
	}).Info("Starting app")

	r := mux.NewRouter()

	r.HandleFunc("/api/calendars/{chapel:[a-zA-Z-]*}/{timezone:[a-zA-Z]*}", RespondIcal)
	r.HandleFunc("/health", RespondHealth)

	http.Handle("/", r)
	http.ListenAndServe(":8080", nil)

	Cache.Purge()
	Cache.Close()

}

func RespondIcal(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	chapel := vars["chapel"]
	tz := vars["timezone"]
	url := base_url + "/" + chapel

	cacheKey := "ical_string_" + chapel + tz

	w.Header().Add("Content-type", "text/calendar; charset=utf-8")
	w.Header().Add("Content-Disposition", "inline; filename="+chapel+".ics")

	if ical_string, err := Cache.Get(cacheKey); err != notFound {
		fmt.Printf("Got it in cache for chapel " + chapel + " / " + tz + "\n")
		fmt.Fprint(w, ical_string)
	} else {
		fmt.Printf("Not in cache for chapel " + chapel + " / " + tz + "\n")

		content, _ := fetchURLContent(url)

		jsonRaw := getJsonFromWebpage(content)

		var data CalData

		json.Unmarshal([]byte(jsonRaw), &data)

		data.Timezone = tz
		data.Chapel = chapel
		data.FetchedUrl = url

		b := bytes.Buffer{}
		goics.NewICalEncode(&b).Encode(data)

		ical_string_local := b.String()
		fmt.Fprint(w, ical_string_local)
		Cache.Set(cacheKey, ical_string_local)
	}

}

// healthcheck endpoint
func RespondHealth(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("ok"))
}

// Fetches content from the URL, returns it back to return string
func fetchURLContent(url string) (string, error) {

	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()

	// TODO : Retry here on retryable errors
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("status error: %v", resp.StatusCode)
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("read body: %v", err)
	}

	return string(data), nil

}

// Given the webpage body, returns the json string
// empty return means nothing found
func getJsonFromWebpage(pageBody string) string {

	scanner := bufio.NewScanner(strings.NewReader(pageBody))
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.TrimRight(line, "\n")
		line = strings.TrimRight(line, "\t")
		line = strings.TrimRight(line, " ")
		line = strings.TrimLeft(line, " ")
		line = strings.TrimLeft(line, "\t")
		// find the line with the json on it
		if strings.HasPrefix(line, "var jsonDataPage") {

			jsonString := ""
			foundEquals := false
			// extract the json
			for i, c := range line {

				// First look for the equals, then the first bracket
				if !foundEquals && string(c) == "=" {
					foundEquals = true
					continue
				}

				if foundEquals && string(c) == "{" {
					jsonString = line[i : len(line)-1]
					break
				}

			}
			return jsonString

		}
	}
	return ""
}

// Object to goics componenter - converts a CalData struct into goics.Componenter
// about to be rendered
func (cal CalData) EmitICal() goics.Componenter {
	c := goics.NewComponent()
	c.SetType("VCALENDAR")
	c.AddProperty("CALSCAL", "GREGORIAN")

	c.AddProperty("VERSION", "2.0")

	c.AddProperty("PRODID", cal.MassTimesString+" - "+cal.ParishChapelName)
	c.AddProperty("X-WR-CALNAME", cal.MassTimesString+" - "+cal.ParishChapelName)

	// TODO : Add better TZ handling here
	if cal.Timezone == "et" {
		c.AddProperty("TZID", "America/New_York")
	} else if cal.Timezone == "ct" {
		c.AddProperty("TZID", "America/Chicago")
	} else if cal.Timezone == "mt" {
		c.AddProperty("TZID", "America/Denver")
	} else if cal.Timezone == "pt" {
		c.AddProperty("TZID", "America/Los_Angeles")
	} else {
		fmt.Println("Must specify timezone")
		// TODO : Add better user facing error handling
		return c
	}

	for i := 0; i < len(cal.MassDays); i++ {

		// Loop through masses
		for k := 0; k < len(cal.MassDays[i].Masses); k++ {

			// TODO - if time is blank in the event, make an all day event

			s := goics.NewComponent()
			s.SetType("VEVENT")

			s.AddProperty("UID", uuid.New().String())

			layout := "2006-01-02"
			startDate, err := time.ParseInLocation(layout, cal.MassDays[i].DayYMD, time.Local)
			if err != nil {
				fmt.Println(err)
			}

			key, val := goics.FormatDateTimeField("DTSTAMP", startDate)
			s.AddProperty(key, val)

			// If time not set, treat as an all day event
			allDay := (len(cal.MassDays[i].Masses[k].TimeATC) == 0)

			if allDay {
				key, val = goics.FormatDateField("DTSTART", startDate)
				s.AddProperty(key, val)
				key, val = goics.FormatDateField("DTEND", startDate)
				s.AddProperty(key, val)
			} else {

				str := cal.MassDays[i].DayYMD + " " + cal.MassDays[i].Masses[k].TimeATC

				// random fixes to make parsing more resilient
				str = strings.Replace(str, "PM", "pm", -1)
				str = strings.Replace(str, "AM", "am", -1)
				str = strings.Replace(str, "on", "pm", -1)
				str = strings.Replace(str, "an", "am", -1)
				str = strings.Replace(str, ";", ":", -1)
				str = strings.Replace(str, "::", ":", -1)
				str = strings.Replace(str, " pm", "pm", -1)
				str = strings.Replace(str, " am", "am", -1)
				str = strings.TrimRight(str, " ")
				str = strings.TrimLeft(str, " ")

				startTime, err := dateparse.ParseLocal(str)
				if err != nil {
					fmt.Println(err)
					// Parse fail events get all day
					key, val = goics.FormatDateField("DTSTART", startDate)
					s.AddProperty(key, val)
					key, val = goics.FormatDateField("DTEND", startDate)
					s.AddProperty(key, val)
				} else {
					key, val = goics.FormatDateTimeField("DTSTART", startTime)
					s.AddProperty(key, val)
					// Duration is 1 hour by default
					key, val = goics.FormatDateTimeField("DTEND", startTime.Add(time.Hour))
					s.AddProperty(key, val)
				}
			}

			s.AddProperty("SUMMARY", cal.MassDays[i].Masses[k].Description)
			s.AddProperty("DESCRIPTION", "Automatically generated from "+cal.FetchedUrl)
			//fmt.Println(cal.MassDays[i].Masses[k].Description)
			c.AddComponent(s)

		}
	}

	return c
}
