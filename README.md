# Docker FSSPX.today ical Feed

Parses schedules from fsspx.today chapel sites, turns into an ical feed you can subscribe to.

## Description

This image runs as a webservice, and you can subscribe to ical feeds.

Currently hardcoded for walton, future project to support dynamic lookup.

URL after running: `http://localhost:8080/api/calendars/ky-walton/et`

Caches responses for 3 hours, in memory. Restart process to flush cache.

## Configuration

Just in the URL itself. The URL takes two params:

  - `http://localhost:8080/api/calendars/[chapel]/[timezone]`

To find the chapel name, grab this portion of the chapel URL:

  - `https://fsspx.today/chapel/[chapel]/`

Timezone should be either `et`, `ct`, `mt`, or `pt`, for the 4 American mainland timezones. The
timezone param ensures that the timezone of the returned ical events is set correctly.

Example URLS:

  - Walton: `/api/calendars/ky-walton/et`
  - Phoenix: `/api/calendars/phoenix/pt`
  - St. Mary's: `/api/calendars/us-ks-stmarys/ct`

## Container Images

The following images are build by CI:

On tags for releases:
 - `registry.gitlab.com/benyanke/docker-fsspx-today-ical:latest`
 - `registry.gitlab.com/benyanke/docker-fsspx-today-ical:{tag}`

On every push to branches:
 - `registry.gitlab.com/benyanke/docker-fsspx-today-ical:branch_{branchname}`
 - `registry.gitlab.com/benyanke/docker-fsspx-today-ical:commit_{commit_hash}`

Note that the `branch_*` and `commit_*` tags are cleaned up regularly.

## Release Process

Test the `commit_*` image after pushing the commit, and do local testing. If it works, make a git tag
matching the pattern `vX.X.X` where `X` is an integer.
