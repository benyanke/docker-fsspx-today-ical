##################
# Build and Test stage
##################
FROM golang:1.22 AS build_base

RUN apt-get update && \
    apt-get install -y \
      git \
    && rm -rf /var/cache/apt/*

# Set the Current Working Directory inside the container
WORKDIR /tmp/app

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

# Get deps and clean up unneeded ones
RUN go mod download

# Copy in app
COPY . .

# Formatting Check - fail if code is not run through go fmt
# Human readable output for easier triage. Then fail build if diff exists
RUN cd /tmp/ ; bash -c "diff -u <(echo -n) <(gofmt -d ./)"

# Unit tests
RUN CGO_ENABLED=0 go test -v

# Build the Go app
RUN go build -o ./out/app .

##################
# Final prod contanier
##################
FROM golang:1.22 as final

WORKDIR /app

RUN apt-get update && \
    apt-get install -y \
      curl \
      dumb-init \
    && rm -rf /var/cache/apt/*

COPY --from=build_base /tmp/app/out/app /app/app

# This container exposes port 8080 to the outside world
EXPOSE 8080

#HEALTHCHECK  --interval=5s --timeout=2s --start-period=10s \
#  CMD curl --fail http://localhost:8080/health/live || exit 1

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/app/app"]
