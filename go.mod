module gitlab.com/benyanke/sspx-calendar-parser

go 1.17

require (
	github.com/ReneKroon/ttlcache/v2 v2.11.0
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/jordic/goics v0.0.0-20210404174824-5a0337b716a0
	github.com/sirupsen/logrus v1.8.1
)

require (
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
)
